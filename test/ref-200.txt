TestSweeper version NA, id NA
input: ../example --type s --dim '100:300:100' foo2
                                    SLATE        SLATE        SLATE         Ref.         Ref.          
type       m       n       k        error    time (ms)      Gflop/s    time (ms)      Gflop/s  status  
   s     100     100     100   1.2346e-15  -----------  -----------  -----------  -----------  pass    
   s     200     200     200   2.4691e-15  -----------  -----------  -----------  -----------  pass    
   s     300     300     300   3.7037e-15  -----------  -----------  -----------  -----------  pass    
All tests passed.
