TestSweeper version NA, id NA
input: ../example --type s --dim '100:300:100x50:200:50' foo3
                                    SLATE        SLATE        SLATE         Ref.         Ref.          
type       m       n       k        error    time (ms)      Gflop/s    time (ms)      Gflop/s  status  
   s     100      50      50   6.1728e-16  -----------  -----------  -----------  -----------  pass    
   s     200     100     100   1.2346e-15  -----------  -----------  -----------  -----------  pass    
   s     300     150     150   1.8518e-15  -----------  -----------  -----------  -----------  pass    
All tests passed.
